import java.util.Scanner;
import java.util.function.DoubleToIntFunction;

import static java.lang.StrictMath.sqrt;

public class Triunghi {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Hello, this program calculates triangle's perimeter and air ");
        //declararea, verificarea si denumirea primei lature
        Tri latura_1 = new Tri();// crearea ca obiect a primei lature
        latura_1.lungime = 0;
        latura_1.NumeLatura = "A";//denumirea laturei
        latura_1.MessageInsertValueLine(); //afisarea mesajului de introducere a laturei
        if (s.hasNextInt()) {
            latura_1.lungime = Integer.parseInt(s.next());
            latura_1.Eroare();
        } else {
            System.out.println("Please be sure that you inserted only digits");
        }
        //finisarea cu prima latura
        //declararea, verificarea si denumirea  latur 2
        Tri latura_2 = new Tri();// crearea ca obiect a primei lature
        latura_2.lungime = 0;
        latura_2.NumeLatura = "B";//denumirea laturei
        latura_2.MessageInsertValueLine(); //afisarea mesajului de introducere a laturei
        if (s.hasNextInt()) {
            latura_2.lungime = Integer.parseInt(s.next());
            latura_2.Eroare();
        } else {
            System.out.println("Please be sure that you inserted only digits");
        }
        //finisarea cu latura 2
        //declararea, verificarea si denumirea  latur 3
        Tri latura_3 = new Tri();// crearea ca obiect a primei lature
        latura_3.lungime = 0;
        latura_3.NumeLatura = "C";//denumirea laturei
        latura_3.MessageInsertValueLine(); //afisarea mesajului de introducere a laturei
        if (s.hasNextInt()) {
            latura_3.lungime = Integer.parseInt(s.next());
            latura_3.Eroare();
        } else {
            System.out.println("Please be sure that you inserted only digits");
        }
        //finisarea cu latura 3
        //vericarea triughiului
        VerificareTriunghi.ValidareTriunghi(latura_1.lungime, latura_2.lungime, latura_3.lungime);
        System.out.println("Perimetru triunghiului reprezinta adumarea celor trei lature deci: ");
        System.out.print( latura_1.lungime + " + "+ latura_2.lungime + " + " + latura_3.lungime + " = ");
        System.out.println(CalculareaTriungi.Perimetru(latura_1.lungime, latura_2.lungime, latura_3.lungime));
        System.out.print("SemiPerimetru triunghiului reprezinta perimetru impartit la 2 si  = "  );
        System.out.println(CalculareaTriungi.SemiPerimetru(latura_1.lungime, latura_2.lungime, latura_3.lungime));
        System.out.print("Aria triunghiului conform teoriei lui HERON = ");
        System.out.println(CalculareaTriungi.Aria(latura_1.lungime, latura_2.lungime, latura_3.lungime));
    }

}
class Tri {
    int lungime;
    String NumeLatura;
    void Eroare(){
        if (lungime>0){
            System.out.println("Lungimra laturei " +  NumeLatura + " este egal cu " + lungime);
        } else {
            System.out.println("ati indrodus o lungime a laturei -" + NumeLatura + "- mai mica decit zero");
        }

    }
    void MessageInsertValueLine(){
        System.out.println("Insert value for line " + NumeLatura);
    }



}
class VerificareTriunghi{
    static void ValidareTriunghi(int a, int b, int c ){
        if ((a <b+c) && (b<a+c) && (c<a+b)){
            System.out.println("lungime laturilor corespunde cu parametri necesari de existenta a unui triunghi");
            if (((a == b) && (a!= c)) || ((b == c) && (b!= a)) ||  ((a==c)&&(b!=c))){
                System.out.println("este un triunghi Isoscel");
            }
            if ((a==c) && (c==b)){
                System.out.println("este un triunghi Echilateral");
            }
            if ((a!=b)&&(b!=c)&&(c!=a)){
                System.out.println("este un triungi Scalen");
            }
        }else{
            System.out.println("lungime laturilor NU corespunde cu parametri necesari de existenta a unui triunghi");
            System.out.println("schimbati lungime  laturilor astfel incit // a <b+c şi b<a+c şi c<a+b //");
        }
    }
}
class CalculareaTriungi{

    static int Perimetru(int a, int b, int c){
        int P = a + b + c;
        return P;
    }
    static double SemiPerimetru(int a, int b, int c){
        double Sp = (a + b + c) /2.f;
         return Sp;
    }
    static double Aria(int a, int b, int c){
        float p = (a + b + c) /2;
        double A = sqrt(p*((p-a)*(p-b)*(p-a)));
        return  A;
    }


}

